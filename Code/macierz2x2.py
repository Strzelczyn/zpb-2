from FindPattern import Wejscie,CostFunction,findSol
import numpy as np


import datetime

macierz1 = [[1, 3], [2,2]]
#macierz1 = [[1,1,2,6], [1,1,6,6],[2,2,4,4],[3,3,5,4]]
#macierz1 = [[1,1,1,1,1,1,1,1], [1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1], [1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1],[1,1,1,1,1,1,1,1]]
macierz1 = [[2,10,16,17, 2,10,18,17],
     [3,11,17,19, 7,14,20,21],
     [4, 5, 2, 7, 8, 9, 6, 3],
     [5,12,10,14, 9,15,13,11],
     [6,13,18,20, 6,13,22,20],
     [7,14,17,21, 3,11,20,19],
     [8, 9, 2, 3, 4, 5, 6, 7],
     [9,15,10,11, 5,12,13,14]]
macierz1 = np.matrix(macierz1)

macierzWejsciowa = Wejscie(macierz1)

wyjscie = CostFunction(macierzWejsciowa,2)
print(findSol(macierzWejsciowa,2))
print("macierz1")
print(macierz1)
print("wyjscie")
print(wyjscie)

print("Typ podobieństwa ilość_mnożeń ilość_elementów_niepasujących")
#for x in macierzWejsciowa.identBest:
  #  print(x)
    
print("Macierze różnic")
#for x in macierzWejsciowa.xRoznBest:
#    print(x)
