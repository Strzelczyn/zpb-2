import numpy as np

def saving(resultMatrix):
    f=open('result.txt','a')
    f.truncate(0)
    f.write('x: \n')
    np.savetxt(f,resultMatrix.x,fmt='% 4d')
    f.write('xRoznBest: \n')
    np.savetxt(f,resultMatrix.xRoznBest,fmt='% 4d')
    f.write('identBest: ' + str(resultMatrix.identBest) + '\n')
    f.write('multiplication: ' + str(resultMatrix.multiplication) + '\n')
    f.write('col: ' + str(resultMatrix.col) + '\n')
    f.write('row: ' + str(resultMatrix.row) + '\n')
    f.close()
    