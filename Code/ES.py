import math
import copy 
import random
from FindPattern import Wejscie,CostFunction,findSol
import numpy as np

def mixing(matrix,k,crosCol,crosRow,childTyp):
    newCol = random.sample(range(max(matrix.col)+1), (max(matrix.col)+1))
    newRow = random.sample(range(max(matrix.row)+1), (max(matrix.row)+1))
    matrixCopy = copy.deepcopy(matrix.x)
    j = 0
    for i in newCol:
        matrix.x[:, matrix.col[j]] = matrixCopy[:, i]
        j += 1
    matrixCopy = copy.deepcopy(matrix.x)
    j = 0
    for i in newRow:
        matrix.x[matrix.row[j], :] = matrixCopy[i, :]
        j += 1
    matrix.col = newCol
    matrix.row = newRow
    if k > 1:
        if childTyp != 3:
            matrixCopy = copy.deepcopy(matrix)
            j = 0 
            for i in crosCol:
                if i != -1:
                    indexI = matrix.col.index(i)
                    temp = matrixCopy.x[:, matrixCopy.col[j]]
                    matrix.x[:, matrix.col[j]] = matrixCopy.x[:, indexI]
                    matrix.x[:, indexI] = temp
                    temp = matrix.col[j]
                    matrix.col[j] = matrix.col[indexI]
                    matrix.col[indexI] = temp
                j += 1
        if childTyp != 2:   
            matrixCopy = copy.deepcopy(matrix)
            j = 0 
            for i in crosRow:
                if i != -1:
                    indexI = matrix.row.index(i)
                    temp = matrixCopy.x[matrixCopy.row[j], :]
                    matrix.x[matrix.row[j], :] = matrixCopy.x[indexI, :]
                    matrix.x[indexI, :] = temp
                    temp = matrix.row[j]
                    matrix.row[j] = matrix.row[indexI]
                    matrix.row[indexI] = temp
                j += 1
      
def evolution(best,best1,population):
    matrixCopy = copy.deepcopy(best.x)
    j = 0
    crosCol = []
    for i in best.col:
        if i == best1.col[j]:
            crosCol.append(i)
        else: 
            crosCol.append(-1)
    j = 0
    crosRow = []
    for i in best.row:
        if i == best1.row[j]:
            crosRow.append(i)
        else: 
            crosRow.append(-1)
    return crosCol,crosRow

def es(orgMatrix,maxDeep):
    N = int(math.sqrt(orgMatrix.x.size)/2)
    population = []
    best = copy.deepcopy(orgMatrix)
    wyjscie = CostFunction(best,maxDeep)
    tmp = findSol(best,maxDeep) 
    best.x = copy.deepcopy(orgMatrix.x)
    best.identBest = tmp[0][0]
    best.multiplication = int(wyjscie + np.sum(tmp[0][2]))
    best.xRoznBest = tmp[1]
    best1 = copy.deepcopy(best)
    crosCol = 0
    crosRow = 0
    if N < 2:
        return best
    for k in range(5):
       for i in range(N):
           population.append(copy.deepcopy(orgMatrix))
       if k > 1:
           crosCol,crosRow = evolution(copy.deepcopy(best),copy.deepcopy(best1),population)
       v = 0
       for item in population:
           childTyp = 0
           if(v < len(population)/2):
               childTyp = 1
           elif v < len(population)*3/4:
               childTyp = 2
           else:
               childTyp = 3
           mixing(item,k,crosCol,crosRow,childTyp)
           wyjscie = CostFunction(item,maxDeep)    
           tmp = findSol(item,maxDeep)
           item.identBest = tmp[0][0]
           item.multiplication = int(wyjscie + np.sum(tmp[0][2]))
           item.xRoznBest = tmp[1]
           v += 1
       population = sorted(population, key=lambda x: x.multiplication)
       if best.multiplication > population[0].multiplication:
           if best.multiplication > population[1].multiplication:
               best1 = copy.deepcopy(population[1])
               best1.x = copy.deepcopy(orgMatrix.x)
           else:
               best1 = copy.deepcopy(best)
           best = copy.deepcopy(population[0])
           best.x = copy.deepcopy(orgMatrix.x)
       else:
           best1 = copy.deepcopy(population[0])
           best1.x = copy.deepcopy(orgMatrix.x)
       population.clear()
    return best
    
