import numpy as np
import random
import math    
import operator
import sys

subMatrix = []
mp = None
mpn = None

class Wejscie:
    def __init__(self, x, ident = 0, identSkala = 0):
        self.x = x
        self.ident = ident
        self.identSkala = identSkala
        self.identBest = []
        self.xRoznBest = []
        self.col = list(range(0, int(math.sqrt(self.x.size))))
        self.row = list(range(0, int(math.sqrt(self.x.size))))
        self.multiplication = 0
        
class SubMatrixPom:
    def __init__(self, ident, jakosc, macierz, macRozn):
        self.ident = ident
        self.jakosc = jakosc
        self.macierz = macierz             
        self.macRozn = macRozn   
        
def findSol(sols,deep):
    bestSol = 0
    #najlepsze rozwiazanie w pierwszym kroku 
    for j in range(len(sols.identBest)):
            if len(str(sols.identBest[j][0])) == 1:
                bestSol = [sols.identBest[j],sols.xRoznBest[j]]
    #bestSol w danym korku 
    for i in range(deep):
        solsInStep = []
        for j in range(len(sols.identBest)):
            if (str(sols.identBest[j][0]).startswith(str(bestSol[0][0])) == True) and (len(str(sols.identBest[j][0])) == len(str(bestSol[0][0]))+2):
                solsInStep.append([sols.identBest[j],sols.xRoznBest[j]])
        for j in solsInStep:
            if(bestSol[0][1]+np.sum(bestSol[0][2])>j[0][1]+np.sum(j[0][2])):
                bestSol = j
    return bestSol

 
#obliczanie liczby mnożeń
def CostFunction(wejscie,deep = 1):
    global subMatrix,mp,mpn
    subRozn = []
    xRob = wejscie.x 
    identWsk = wejscie.ident
    identSkala = wejscie.identSkala

    wx,kx = xRob.shape
    wxpol = int(wx/2)
    kxpol = int(kx/2)
    wx2pol = int((wxpol**2)/2)
    jakosc = 0
    jakoscPoz = 0
    jakoscPion = 0
    jakoscSkoAbs = 0
    jakoscSkosDod = 0
    jakoscSkosUjm = 0
    jakoscRozn = 0
    bazaMn = wxpol*wxpol
    
    if wxpol >= 1: #gdy jest to wciąż macierz, a nie tylko liczba    

        #podział macierzy na ćwiartki
        xPol1 = xRob[0:wxpol, 0:wxpol] #lewy góra
        xPol2 = xRob[0:wxpol, wxpol:wx] #prawy góra
        xPol3 = xRob[wxpol:wx, 0:wxpol] #lewy dół
        xPol4 = xRob[wxpol:wx, wxpol:wx] #prawy dół
        
        jeden = np.ones((wxpol, wxpol)) #macierz jedynkowa
        los = np.random.random((wxpol, wxpol)) #macierz losowa
        
        #ilosc elementow podobnych w powiazanych cwiartkach
        mp = np.zeros((4, 4))  #macierz podobienstw 
        #ilosc elementow podobnych z przeciwnym znakiem w powiazanych cwiartkach
        mpn = np.zeros((4, 4)) #macierz podobienstw
        
        #1: podobienstwo poziome %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        identCur = identWsk + 1 #wskaznik podobienstwa podmacierzy
        ident = []
        ident.append(identCur)
        wejscie.identSkala = identSkala + 2 #bo podmacierz ma juz wsk*10
        
        #sprawdz gdzie nie wystepuja zera
        gdzieNieZera12 = (xPol1 != 0) & (xPol2 != 0)
        gdzieNieZera34 = (xPol3 != 0) & (xPol4 != 0)
        
        # wyznaczenie ile elementów jest niepodobnych minus zerowe
        #podobienstwo wprost macierzy
        mPod12 = xPol1 == xPol2
        mp[0,1] = np.sum(np.sum(mPod12))
        #podobienstwo z minusem macierzy
        mPodn12 = xPol1 == -xPol2
        mPodn12 = mPod12 & ~(xPol1 == -xPol2)
        mpn[0,1] = np.sum(np.sum(mPodn12))
        #macierz roznic
        mRozn12 = ~(mPod12 | mPodn12)
        ileRozn12 = np.sum(np.sum(mRozn12))
    
        #podobienstwo wprost macierzy
        mPod34 = xPol3 == xPol4
        mp[2,3] = np.sum(np.sum(mPod34))
        #podobienstwo z minusem macierzy
        mPodn34 = xPol3 == -xPol4
        mPodn34 = mPod34 & ~(xPol3 == -xPol4)
        mpn[2,3] = np.sum(np.sum(mPodn34))
        #macierz roznic
        mRozn34 = ~(mPod34 | mPodn34)
        #ileRozn34 = sum(sum(mRozn34 & gdzieNieZera34))
        ileRozn34 = np.sum(np.sum(mRozn34))
    
        # wyznaczenie ile elementów jest niepodobnych minus zerowe
        ilMnoPoz12 = 2 * (bazaMn - mp[0,1] - mpn[0,1]) - ileRozn12
        ilMnoPoz34 = 2 * (bazaMn - mp[2,3] - mpn[2,3]) - ileRozn34
        
        ilMnoPoz12 = np.where(ilMnoPoz12 > 0, ilMnoPoz12, 0)
        ilMnoPoz34 = np.where(ilMnoPoz34 > 0, ilMnoPoz34, 0)
        # zwiekszenie jakosci o ilosc elementow niepodobnych
        
        #bierze cala macierz A z zerami w miejscach gdzie choc jedno zero wystapilo
        #dzieki zastosowaniu macierzy roznic
        #macierz A z symetria dodatnia  
        xApozD = xPol1 * (1-mPodn12) * gdzieNieZera12
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xApozD!=0))
            jakoscPoz = jakoscPoz + jakoscPom
        else:
            if np.sum(np.sum(abs(xApozD))) != 0:
                jakoscPoz += qualitySubarray(wejscie,xApozD,identCur,10,deep)
        #macierz A z symetria ujemna  
        xApozU = xPol1 * mPodn12 * gdzieNieZera12
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xApozU!=0))
            jakoscPoz = jakoscPoz + jakoscPom
        else:
            if np.sum(np.sum(abs(xApozU))) != 0:
                jakoscPoz += qualitySubarray(wejscie,xApozU,identCur,20,deep)
        #bierze cala macierz A z zerami w miejscach gdzie choc jedno zero wystapilo
        #dzieki zastosowaniu macierzy roznic
        #macierz B z symetria dodatnia  
        xBpozD = xPol3 * (1-mPodn34) * gdzieNieZera34
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xBpozD!=0))
            jakoscPoz = jakoscPoz + jakoscPom
        else:
            if np.sum(np.sum(abs(xBpozD))) != 0:
                jakoscPoz += qualitySubarray(wejscie,xBpozD,identCur,30,deep)
        #macierz B z symetria ujemna  
        xBpozU = xPol3 * mPodn34 * gdzieNieZera34
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xBpozU!=0))
            jakoscPoz = jakoscPoz + jakoscPom
        else:
            if np.sum(np.sum(abs(xBpozU))) != 0:
                jakoscPoz += qualitySubarray(wejscie,xBpozU,identCur,40,deep)

        xRozn = np.zeros((wxpol+wxpol,wxpol+wxpol))
        xRozn[0:wxpol,wxpol:wxpol+wxpol] = (xPol2 - xPol1) * mRozn12
        xRozn[wxpol:wxpol+wxpol,wxpol:wxpol+wxpol] = (xPol4 - xPol3) * mRozn34
        subRozn.append(xRozn)

        if jakoscPoz == 0:
            jakoscPoz = np.inf
        elif np.sum(np.sum(np.absolute(xRozn))) != 0:
            if deep == 0:
                #wywolanie funkcji jakosci dla podmacierzy B
                #obliczenie ile elementow niezerowych
                jakoscPom = np.sum(np.sum(xRozn!=0))
                jakoscPoz = jakoscPoz + jakoscPom
            else:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscPoz += qualitySubarray(wejscie,np.array(xRozn),identCur,90,deep)
    
        if deep != 0 and jakoscPoz > 0:            
            subMatrixPom = SubMatrixPom(identCur,jakoscPoz,xRob,xRozn)
            subMatrix.append(subMatrixPom)

    
        ##2: podobienstwo pionowe %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        identCur = identWsk + 2 #wskaznik podobienstwa podmacierzy
        ident.append(identCur)
        wejscie.identSkala = identSkala + 2 #bo podmacierz
    
        #przywrocenie oryginalnej xRob (mogla sie zmienic w trakcie optymalizacji
        xPol1 = xRob[0:wxpol, 0:wxpol] #lewy góra
        xPol2 = xRob[0:wxpol, wxpol:wx] #prawy góra
        xPol3 = xRob[wxpol:wx, 0:wxpol] #lewy dół
        xPol4 = xRob[wxpol:wx, wxpol:wx] #prawy dół
    
        #sprawdz gdzie nie wystepuja zera
        gdzieNieZera13 = (xPol1 != 0) & (xPol3 != 0)
        gdzieNieZera24 = (xPol2 != 0) & (xPol4 != 0)
        
        #podobienstwo wprost macierzy
        mPod13 = xPol1 == xPol3
        mp[0,2] = np.sum(np.sum(mPod13))
        #podobienstwo z minusem macierzy
        mPodn13 = xPol1 == -xPol3
        mPodn13 = mPodn13 & ~(xPol1 == -xPol3)
        mpn[0,2] = np.sum(np.sum(mPodn13))
        #macierz roznic
        mRozn13 = ~(mPod13 | mPodn13)
        ileRozn13 = np.sum(np.sum(mRozn13))
        
        #podobienstwo wprost macierzy
        mPod24 = xPol2==xPol4
        mp[1,3] = np.sum(np.sum(mPod24))
        #podobienstwo z minusem macierzy
        mPodn24 = xPol2==-xPol4
        mPodn24 = mPodn24 & ~(xPol2 == -xPol4)
        mpn[1,3] = np.sum(np.sum(mPodn24))
        #macierz roznic
        mRozn24 = ~(mPod24 | mPodn24)
        ileRozn24 = np.sum(np.sum(mRozn24))
        
        # wyznaczenie ile elementów jest niepodobnych minus zerowe
        ilMnoPion13 = 2 * (bazaMn - mp[0,2] - mpn[0,2]) - ileRozn13
        ilMnoPion24 = 2 * (bazaMn - mp[1,3] - mpn[1,3]) - ileRozn24
        # zwiekszenie jakosci o ilosc elementow niepodobnych
        
        #bierze cala macierz A z zerami w miejscach gdzie choc jedno zero wystapilo
        #dzieki zastosowaniu macierzy roznic
        #macierz A z symetria dodatnia  
        xApionD = xPol1 * (1-mPodn13) * gdzieNieZera13
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xApionD!=0))
            jakoscPion = jakoscPion + jakoscPom
        else:
            if np.sum(np.sum(abs(xApionD))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy A
                jakoscPion += qualitySubarray(wejscie,xApozD,identCur,10,deep)
        #macierz A z symetria ujemna  
        xApionU = xPol1 * mPodn13 * gdzieNieZera13
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xApionU!=0))
            jakoscPion = jakoscPion + jakoscPom
        else:
            if np.sum(np.sum(abs(xApionU))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy A
                jakoscPion += qualitySubarray(wejscie,xApionU,identCur,20,deep)
        #macierz B z symetria dodatnia  
        xBpionD = xPol2 * (1-mPodn24) * gdzieNieZera24
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xBpionD!=0))
            jakoscPion = jakoscPion + jakoscPom
        else:
            if np.sum(np.sum(abs(xBpionD))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscPion += qualitySubarray(wejscie,xBpionD,identCur,30,deep)
        #macierz B z symetria ujemna  
        xBpionU = xPol2 * mPodn24 * gdzieNieZera24
        if deep == 0:  
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xBpionU!=0))
            jakoscPion = jakoscPion + jakoscPom
        else:
            if np.sum(np.sum(abs(xBpionU))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscPion += qualitySubarray(wejscie,xBpionU,identCur,40,deep)
            
        #oblicz ile operacji potrzeba wykonac dla macierzy roznic
        xRozn13 = (xPol3 - xPol1)*mRozn13
        xRozn24 = (xPol4 - xPol2)*mRozn24
        xRozn = np.zeros((wxpol+xRozn13.shape[0],wx))
        xRozn[wxpol:wxpol+wxpol,0:wxpol] = xRozn13
        xRozn[wxpol:wxpol+wxpol,wxpol:wxpol+xRozn24.shape[0]] = xRozn24
        subRozn.append(xRozn)

        if jakoscPion == 0:
            jakoscPion = np.inf
        elif np.sum(np.sum(np.absolute(xRozn))) != 0:
            if deep == 0:
                #wywolanie funkcji jakosci dla podmacierzy B
                #obliczenie ile elementow niezerowych
                jakoscPom = np.sum(np.sum(xRozn!=0))
                jakoscPion = jakoscPion + jakoscPom
            else:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscPion += qualitySubarray(wejscie,np.array(xRozn),identCur,90,deep)

        if deep != 0 and jakoscPion > 0:
            subMatrixPom = SubMatrixPom(identCur,jakoscPion,xRob,xRozn)
            subMatrix.append(subMatrixPom)
            
        # 3: podobienstwo skosne absolutna typu [A B;-B A] [A B;B -A] %%%%%%%%%%%
        identCur = identWsk + 3 #wskaznik podobienstwa podmacierzy
        ident.append(identCur)
        wejscie.identSkala = identSkala + 2 #bo podmacierz
        
        #przywrocenie oryginalnej xRob (mogla sie zmienic w trakcie optymalizacji
        xPol1 = xRob[0:wxpol, 0:wxpol] #lewy góra
        xPol2 = xRob[0:wxpol, wxpol:wx] #prawy góra
        xPol3 = xRob[wxpol:wx, 0:wxpol] #lewy dół
        xPol4 = xRob[wxpol:wx, wxpol:wx] #prawy dół
    
        #sprawdz gdzie wystepuja zera
        gdzieNieZera14 = (xPol1 != 0) + (xPol4 != 0)
        gdzieNieZera23 = (xPol2 != 0) + (xPol3 != 0)
        
        #podobienstwo wprost macierzy A==D dla: [A B;C D]
        mPod14 = xPol1 == xPol4
        mp[0,3] = np.sum(np.sum(mPod14))
        #podobienstwo z minusem macierzy A== -D dla: [A B;C D]
        mPodn14 = xPol1 == -xPol4
        mpn[0,3] = np.sum(np.sum(mPodn14))
    
        #podobienstwo wprost macierzy  B==C dla: [A B;C D]
        mPod23 = xPol2 == xPol3
        mp[1,2] = np.sum(np.sum(mPod23))
        #podobienstwo z minusem macierzy  B== -C dla: [A B;C D]
        mPodn23 = xPol2 == -xPol3
        mpn[1,2] = np.sum(np.sum(mPodn23))
        
        # znajdz najwieksze podobienstwo
        mpMax  = max([mp[0,3], mp[1,2], mpn[0,3], mpn[1,2]])
        gdzieMax = [mp[0,3], mp[1,2], mpn[0,3], mpn[1,2]].index(mpMax)
        
        # wyznaczenie jakosci z uwzglednieniem ilosc elementow niepodobnych
        ilMnoSkos = bazaMn - mpMax 
        
        if gdzieMax == 0: #  A==D dla: [A B;C D]
            macierzA = xPol1
            #ile zer wsrod niepodobnych
            ileRoznAbs = np.sum(np.sum((xPol1!=xPol4) & gdzieNieZera14))
            macierzB = xPol2
            macierzC = xPol3
            mPod14 = xPol1 == xPol4
            macierzPod = mPod14
            mRozn = np.zeros((wxpol+wxpol,wxpol+wxpol))
            mRozn[0:wxpol,0:wxpol] =~mPod14
            mRozn[wxpol:wxpol+wxpol,wxpol:wxpol+wxpol] = ~mPod14
            xRozn = np.zeros((wxpol+wxpol,wx))
            xRozn[wxpol:wxpol+wxpol,wxpol:wxpol+wxpol] = (xPol4 - xPol1)* ~mPod14
            subRozn.append(xRozn)
        elif gdzieMax == 1: #  B==C dla: [A B;C D]
            macierzA = xPol2
            #ile zer wsrod niepodobnych
            ileRoznAbs = np.sum(np.sum((xPol2!=xPol3) & gdzieNieZera23))
            macierzB = xPol1
            macierzC = xPol4
            mPod23 = xPol2 == xPol3
            macierzPod = mPod23
            mRozn = np.zeros((wxpol+wxpol,wxpol+wxpol))
            mRozn[0:wxpol,wxpol:wxpol+wxpol] = ~mPod23
            mRozn[wxpol:wxpol+wxpol,0:wxpol] = ~mPod23
            xRozn = np.zeros((wxpol+wxpol,wx))
            xRozn[wxpol:wxpol+wxpol,0:wxpol] = (xPol3 - xPol2) * ~mPod23
            subRozn.append(xRozn)
        elif gdzieMax == 2: #  A== -D dla: [A B;C D]
            macierzA = xPol1
            #ile zer wsrod niepodobnych
            ileRoznAbs = np.sum(np.sum((xPol1!=-xPol4) & gdzieNieZera14))
            macierzB = xPol2
            macierzC = -xPol3 #bo zamieniam [A B;C -A] = {A B;-C A]
            macierzPod = mPodn14
            mRozn = np.zeros((wxpol+wxpol,wxpol+wxpol))
            mRozn[0:wxpol,0:wxpol] = ~mPodn14
            mRozn[wxpol:wxpol+wxpol,wxpol:wxpol+wxpol] = ~mPodn14
            #oblicz ile operacji potrzeba wykonac dla macierzy roznic
            xRozn = np.zeros((wxpol+wxpol,wx))
            xRozn[wxpol:wxpol+wxpol,wxpol:wxpol+wxpol] = (xPol4 - xPol1)*~mPodn14
            subRozn.append(xRozn)
        elif gdzieMax == 3: # B== -C dla: [A B;C D]
            macierzA = xPol2
            #ile zer wsrod niepodobnych
            ileRoznAbs = np.sum(np.sum((xPol2!=-xPol3) & gdzieNieZera23))
            macierzB = xPol1
            macierzC = -xPol4 #bo zamieniam [A B;-B D] = {A B;B -D]
            macierzPod = mPodn23
            mRozn = np.zeros((wxpol+wxpol,wxpol+wxpol))
            mRozn[0:wxpol,wxpol:wxpol+wxpol] = ~mPodn23
            mRozn[wxpol:wxpol+wxpol,0:wxpol] = ~mPodn23
            #oblicz ile operacji potrzeba wykonac dla macierzy roznic
            xRozn = np.zeros((wxpol+wxpol,wx))
            xRozn[wxpol:wxpol+wxpol,0:wxpol] = (xPol3 - xPol2) * ~mPodn23
            subRozn.append(xRozn)

        xCmA = macierzC - macierzA
        if deep == 0:
            jakoscPom = np.sum(np.sum(xCmA!=0))
            jakoscSkoAbs = jakoscSkoAbs + jakoscPom
        else:
            if np.sum(np.sum(abs(xCmA))) != 0: #%%%%%%% tu sprawdzic    
                #wywolanie funkcji jakosci dla sumy podmacierzy
                jakoscSkoAbs += qualitySubarray(wejscie,xCmA,identCur,10,deep)
        xBmA = macierzB - macierzA
        if deep == 0:
            jakoscPom = np.sum(np.sum(xBmA!=0))
            jakoscSkoAbs = jakoscSkoAbs + jakoscPom
        else:
            if np.sum(np.sum(abs(xBmA))) != 0: #%%%%%%% tu sprawdzic
               #wywolanie funkcji jakosci dla roznicy podmacierzy
               jakoscSkoAbs += qualitySubarray(wejscie,xBmA,identCur,20,deep)
        xA = macierzA
        if deep == 0:
            jakoscPom = np.sum(np.sum(xA!=0))
            jakoscSkoAbs = jakoscSkoAbs + jakoscPom
        else:
            if np.sum(np.sum(abs(xA))) != 0: #%%%%%%% tu sprawdzic
                jakoscSkoAbs += qualitySubarray(wejscie,xA,identCur,30,deep)
            
        if jakoscSkoAbs == 0:
            jakoscSkoAbs = np.inf
        elif np.sum(np.sum(abs(xRozn))) != 0:
            if deep == 0:
                #wywolanie funkcji jakosci dla podmacierzy B
                #obliczenie ile elementow niezerowych
                jakoscPom = np.sum(np.sum(xRozn!=0))
                jakoscSkoAbs = jakoscSkoAbs + jakoscPom
            else:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscSkoAbs += qualitySubarray(wejscie,xRozn,identCur,90,deep)

        if deep != 0 and jakoscSkoAbs > 0:
            subMatrixPom = SubMatrixPom(identCur,jakoscSkoAbs,xRob,xRozn)
            subMatrix.append(subMatrixPom)
            
        # 4: podobienstwo skosne bezwzgledna typu [A B;B A]  %%%%%%%%%%%%%%%%%%
        identCur = identWsk + 4 #wskaznik podobienstwa podmacierzy
        ident.append(identCur)
        wejscie.identSkala = identSkala + 2 #bo podmacierz
    
        #przywrocenie oryginalnej xRob (mogla sie zmienic w trakcie optymalizacji
        xPol1 = xRob[0:wxpol, 0:wxpol] #lewy góra
        xPol2 = xRob[0:wxpol, wxpol:wx] #prawy góra
        xPol3 = xRob[wxpol:wx, 0:wxpol] #lewy dół
        xPol4 = xRob[wxpol:wx, wxpol:wx] #prawy dół
        
        # #%%%%%%%%%%%%%%%%%%%%%%%%
        #podobienstwo wprost macierzy A==D dla: [A B;C D]
        mPod14 = xPol1 == xPol4
        mp[0,3] = np.sum(np.sum(mPod14))
    
        #podobienstwo wprost macierzy  B==C dla: [A B;C D]
        mPod23 = xPol2==xPol3
        mp[1,2] = np.sum(np.sum(mPod23))
    
        #ile zer wsrod niepodobnych
        mRozn14 = ~mPod14
        mRozn23 = ~mPod23
        ileRoznDod14 = np.sum(np.sum(mRozn14))
        ileRoznDod23 = np.sum(np.sum(mRozn23))
        
        #oblicz ile operacji potrzeba wykonac dla macierzy roznic
        xRozn = np.zeros((wx,wx))
        xRozn[wxpol:wx,0:wxpol] = (xPol3 - xPol2)*mRozn23
        xRozn[wxpol:wx,wxpol:wx] = (xPol4 - xPol1)*mRozn14
        subRozn.append(xRozn)
        # wyznaczenie ile elementów jest niepodobnych minus zerowe
        ilMnoSkosBDod14 = bazaMn - mp[0,3]
        ilMnoSkosBDod23 = bazaMn - mp[1,2]
        
        # zwiekszenie jakosci o ilosc elementow niepodobnych
    
        xApBbezw = xPol1 + xPol2
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xApBbezw!=0))
            jakoscSkosDod = jakoscSkosDod + jakoscPom
        else:
            if np.sum(np.sum(abs(xApBbezw))) != 0: # %%%%%%% tu sprawdzic
                #wywolanie funkcji jakosci dla sumy podmacierzy
                jakoscSkosDod += qualitySubarray(wejscie,xApBbezw,identCur,10,deep)
        xAmBbezw = xPol1 - xPol2
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xAmBbezw!=0))
            jakoscSkosDod = jakoscSkosDod + jakoscPom
        else:
            if np.sum(np.sum(abs(xAmBbezw))) != 0: # %%%%%%% tu sprawdzic
                #wywolanie funkcji jakosci dla roznicy podmacierzy
                jakoscSkosDod += qualitySubarray(wejscie,xAmBbezw,identCur,20,deep)
        if jakoscSkosDod == 0:
            jakoscSkosDod = np.inf
        elif np.sum(np.sum(abs(xRozn))) != 0:
            if deep == 0:
                #wywolanie funkcji jakosci dla podmacierzy B
                #obliczenie ile elementow niezerowych
                jakoscPom = np.sum(np.sum(xRozn!=0))
                jakoscSkosDod = jakoscSkosDod + jakoscPom
            else:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscSkosDod += qualitySubarray(wejscie,xRozn,identCur,90,deep)
    
        if deep != 0 and jakoscSkosDod > 0:
            subMatrixPom = SubMatrixPom(identCur,jakoscSkosDod,xRob,xRozn)
            subMatrix.append(subMatrixPom)
    
        # 5: podobienstwo skosne ujemne bezwzgledna typu [A B;-B -A]  %%%%%%%%%
        identCur = identWsk + 5 #wskaznik podobienstwa podmacierzy
        ident.append(identCur)
        wejscie.identSkala = identSkala + 2 #bo podmacierz
           
        #przywrocenie oryginalnej xRob (mogla sie zmienic w trakcie optymalizacji
        xPol1 = xRob[0:wxpol, 0:wxpol] #lewy góra
        xPol2 = xRob[0:wxpol, wxpol:wx] #prawy góra
        xPol3 = xRob[wxpol:wx, 0:wxpol] #lewy dół
        xPol4 = xRob[wxpol:wx, wxpol:wx] #prawy dół
        
        #podobienstwo z minusem macierzy A== -D dla: [A B;C D]
        mPodn14 = xPol1 == -xPol4
        mpn[0,3] = np.sum(np.sum(mPodn14))
    
        #podobienstwo z minusem macierzy  B== -C dla: [A B;C D]
        mPodn23 = xPol2 == -xPol3
        mpn[1,2] = np.sum(np.sum(mPodn23))

        #ile zer wsrod niepodobnych
        mRoznUjm14 = ~mPodn14
        mRoznUjm23 = ~mPodn23
        ileRoznUjm14 = np.sum(np.sum(mRoznUjm14))
        ileRoznUjm23 = np.sum(np.sum(mRoznUjm23))
        
        #oblicz ile operacji potrzeba wykonac dla macierzy roznic
        xRoznUjm23 = (-xPol3 - xPol2)* mRoznUjm23
        xRoznUjm14 = (-xPol4 - xPol1)* mRoznUjm14
        xRozn = np.zeros((wx,wx))
        xRozn[wxpol:wx,0:wxpol] = xRoznUjm23
        xRozn[wxpol:wx,wxpol:wx] = xRoznUjm14
        subRozn.append(xRozn)

        # wyznaczenie ile elementów jest niepodobnych minus zerowe
        ilMnoSkosBUjm14 = bazaMn - mpn[0,3]
        ilMnoSkosBUjm23 = bazaMn - mpn[1,2]
        #zwiekszenie jakosci o ilosc elementow niepodobnych
    
        xApBbezw = xPol1 + xPol2
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xApBbezw!=0))
            jakoscSkosUjm = jakoscSkosUjm + jakoscPom
        else:
            if np.sum(np.sum(abs(xApBbezw))) != 0: #%%%%%%% tu sprawdzic
                #wywolanie funkcji jakosci dla sumy podmacierzy
                jakoscSkosUjm += qualitySubarray(wejscie,xApBbezw,identCur,10,deep)

        xAmBbezw = xPol1 - xPol2
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xAmBbezw!=0))
            jakoscSkosUjm = jakoscSkosUjm + jakoscPom
        else:
            if np.sum(np.sum(abs(xAmBbezw))) != 0: #%%%%%%% tu sprawdzic
                #wywolanie funkcji jakosci dla roznicy podmacierzy
                jakoscSkosUjm += qualitySubarray(wejscie,xApBbezw,identCur,20,deep)

        if jakoscSkosUjm == 0:
            jakoscSkosUjm = np.inf
        elif np.sum(np.sum(abs(xRozn))) != 0:
            if deep == 0:   
                #obliczenie ile elementow niezerowych
                jakoscPom = np.sum(np.sum(xRozn!=0))
                jakoscSkosUjm = jakoscSkosUjm + jakoscPom
            else:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscSkosUjm += qualitySubarray(wejscie,xRozn,identCur,90,deep)
    
        if deep != 0 and jakoscSkosUjm > 0:
            subMatrixPom = SubMatrixPom(identCur,jakoscSkosUjm,xRob,xRozn)
            subMatrix.append(subMatrixPom)
        
        # 6: brak podobienstwa
        identCur = identWsk + 6 #wskaznik podobienstwa podmacierzy
        ident.append(identCur)
        wejscie.identSkala = identSkala + 2 #bo podmacierz
    
        #przywrocenie oryginalnej xRob (mogla sie zmienic w trakcie optymalizacji
        xPol1 = xRob[0:wxpol, 0:wxpol] #lewy góra
        xPol2 = xRob[0:wxpol, wxpol:wx] #prawy góra
        xPol3 = xRob[wxpol:wx, 0:wxpol] #lewy dół
        xPol4 = xRob[wxpol:wx, wxpol:wx] #prawy dół
    
        xA = xPol1
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xA!=0))
            jakoscRozn = jakoscRozn + jakoscPom
        else:
            if np.sum(np.sum(abs(xA))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy A
                jakoscRozn += qualitySubarray(wejscie,xA,identCur,10,deep)
            
        xB = xPol2
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xB!=0))
            jakoscRozn = jakoscRozn + jakoscPom
        else:
            if np.sum(np.sum(abs(xB))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy B
                jakoscRozn += qualitySubarray(wejscie,xB,identCur,20,deep)
        xC = xPol3
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xC!=0))
            jakoscRozn = jakoscRozn + jakoscPom
        else:  
            if np.sum(np.sum(abs(xC))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy C
                jakoscRozn += qualitySubarray(wejscie,xC,identCur,30,deep)
    
        xD = xPol4
        if deep == 0:
            #obliczenie ile elementow niezerowych
            jakoscPom = np.sum(np.sum(xD!=0))
            jakoscRozn = jakoscRozn + jakoscPom
        else:  
            if np.sum(np.sum(abs(xD))) != 0:
                #wywolanie funkcji jakosci dla podmacierzy D
                jakoscRozn += qualitySubarray(wejscie,xD,identCur,40,deep)
    
        if deep != 0 and jakoscRozn > 0:
            subMatrixPom = SubMatrixPom(identCur,jakoscRozn,xRob,0)
            subMatrix.append(subMatrixPom)
      
        ##%% %%%%
        ileMno = np.zeros((6,2))
        ileMno[0,0] = ilMnoPoz12
        ileMno[0,1] = ilMnoPoz34
        ileMno[1,0] = ilMnoPion13
        ileMno[1,1] = ilMnoPion24
        ileMno[2,0] = ilMnoSkos
        ileMno[3,0] = ilMnoSkosBDod14
        ileMno[3,1] = ilMnoSkosBDod23
        ileMno[4,0] = ilMnoSkosBUjm14
        ileMno[4,1] = ilMnoSkosBUjm23

    else:
        jakosc = 1
        ileMno = [-1, -1]
    #???????????????
    wyjscie  = min([jakoscPoz, jakoscPion, jakoscSkoAbs, jakoscSkosDod, jakoscSkosUjm, jakoscRozn])
    ktoBest = [jakoscPoz, jakoscPion, jakoscSkoAbs, jakoscSkosDod, jakoscSkosUjm, jakoscRozn].index(wyjscie)
    if wyjscie != 0:
       wejscie.identBest.append([ident[ktoBest], wyjscie, ileMno[ktoBest,:]])
       if xRozn is None or ktoBest > 4:
           wejscie.xRoznBest.append([])
       else:
           wejscie.xRoznBest.append(subRozn[ktoBest])
    return wyjscie + jakosc
    
def qualitySubarray(wejscie,macierzSymetri,identCur,addIdent,deep):
    #wywolanie funkcji jakosci dla podmacierzy 
    wejscie.x = macierzSymetri
    wejscie.ident = identCur * 100 + addIdent
    if addIdent == 90:
        jakoscPom = CostFunction(wejscie,0)
    else:
        jakoscPom = CostFunction(wejscie,deep-1)
    #return 0
    return jakoscPom
