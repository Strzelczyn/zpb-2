import math
import copy 
import random
from random import randrange
from FindPattern import Wejscie,CostFunction,findSol
import numpy as np

def mixing(matrix):
    pos1 = random.randrange(max(matrix.col)) 
    pos2 = random.randrange(max(matrix.col)) 
    matrix.col[pos1], matrix.col[pos2] = matrix.col[pos2], matrix.col[pos1] 
    temp = copy.deepcopy(matrix.x[:, pos1])
    matrix.x[:, pos1] = matrix.x[:, pos2]
    matrix.x[:, pos2] = temp
    pos1 = random.randrange(max(matrix.row)) 
    pos2 = random.randrange(max(matrix.row)) 
    matrix.row[pos1], matrix.row[pos2] = matrix.row[pos2], matrix.row[pos1] 
    temp = copy.deepcopy(matrix.x[pos1,:])
    matrix.x[pos1,:] = matrix.x[pos2,:]
    matrix.x[pos2,:] = temp

def annealing(orgMatrix,maxDeep):
    N = int(math.sqrt(orgMatrix.x.size))
    Tmin = int(math.sqrt(orgMatrix.x.size)/2)
    best = copy.deepcopy(orgMatrix)
    wyjscie = CostFunction(best,maxDeep)    
    tmp = findSol(best,maxDeep) 
    best.x = copy.deepcopy(orgMatrix.x)
    best.identBest = tmp[0][0]
    best.multiplication = int(wyjscie + np.sum(tmp[0][2]))
    best.xRoznBest = tmp[1]
    T = best.multiplication
    i = 0
    while T > Tmin and N > i:
        new = copy.deepcopy(orgMatrix)
        mixing(new)
        wyjscie = CostFunction(new,maxDeep)    
        tmp = findSol(new,maxDeep) 
        new.identBest = tmp[0][0]
        new.multiplication = int(wyjscie + np.sum(tmp[0][2]))
        new.xRoznBest = tmp[1]
        if new.multiplication < best.multiplication:
            best = copy.deepcopy(new)
            best.x = copy.deepcopy(orgMatrix.x)
        elif randrange(2) < math.e**-((new.multiplication-best.multiplication)/T):
            best = copy.deepcopy(new)
            best.x = copy.deepcopy(orgMatrix.x)
        T = best.multiplication
        i += 1
    return best