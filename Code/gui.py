from tkinter import *
from tkinter import ttk
from FindPattern import Wejscie,CostFunction,findSol
from Saving import saving
from Annealing import annealing
from ES import es
import numpy as np

def findResult():
    text = E1.get("1.0","end")
    text.replace(' ','')
    rows = list(filter(None, text.split('\n')))
    macierz1 = [row.split(',') for row in rows]
    macierz1 = [[float(y) for y in x] for x in macierz1]
    macierz1 = np.matrix(macierz1)
    macierzWejsciowa = Wejscie(macierz1)
    if comboExample.get() == "ES":
        saving(es(macierzWejsciowa,int(comboExample1.get())))
        print("ES")
    if comboExample.get() == "Wyżarzanie":
        saving(annealing(macierzWejsciowa,int(comboExample1.get())))
        print("Annealing")

if __name__ == "__main__": 
    root = Tk()
    root.geometry("450x550")
    root.resizable(False, False)
    L1 = Label(root, text="Algorytm")
    L1.place(x=200, y=20)
    comboExample = ttk.Combobox(root, values=["ES", "Wyżarzanie",],)
    comboExample.current(0)
    comboExample.place(x=150, y=50)
    L2 = Label(root, text="Głębokość")
    L2.place(x=200, y=100)
    comboExample1 = ttk.Combobox(root, values=["0","1","2","3","4","5","6","7","8","9"],)
    comboExample1.current(0)
    comboExample1.place(x=150, y=150)
    L3 = Label(root, text="Macierz")
    L3.place(x=203, y=200)
    E1 = Text(root, height=10, width=40)
    #E1.insert(END,"2,10,16,17,2,10,18,17\n3,11,17,19,7,14,20,21\n4,5,2,7,8,9,6,3\n5,12,10,14,9,15,13,11\n6,13,18,20,6,13,22,20\n7,14,17,21,3,11,20,19\n8,9,2,3,4,5,6,7\n9,15,10,11,5,12,13,14")
    E1.insert(END,"1,5,5,4\n3,4,5,3\n9,4,3,5\n1,8,1,4")
    E1.place(x=20, y=250)
    TH = Button(root,text="Oblicz", command=findResult)
    TH.place(x=200, y=480)
    root.mainloop()

